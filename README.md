# Graphs #


### Technology ###

* HTML
* CSS
* Javascript
* Bootstrap

### Features ###

* Add data to graphs
* generates bar graph based on data.
* Histogram & pie graph not implemented.

### Problems ###

* Usage of canvas in JS is old.

### Scope ###

* Use of chartJS to allow users to create better graph images.